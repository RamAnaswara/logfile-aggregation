# README
This program aggregates log messages based on transaction id.

Sample input file content:


```

"time":"1","transactionId":"123","message":"User x logging in"
"time":"2","transactionId":"124","message":"User y logging in"
"time":"3","transactionId":"125","message":"User z logging in"
"time":"4","transactionId":"123","message":"User x logging out"
"time":"5","transactionId":"124","message":"User y logging out"
"time":"6","transactionId":"125","message":"User z resetting password"
```



Output:

```
123:
		User x logging in
		User x logging out
124:
		User y logging in
		User y logging out
125:
		User z logging in
		User z resetting password
```