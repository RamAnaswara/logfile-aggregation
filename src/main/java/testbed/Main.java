package testbed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    private static final String FILE_PATH = "sample.audit.log";

    public static void main( String[] args ) throws IOException {
        Pattern p = Pattern.compile("(.*)\"transactionId\":\"(.*?)\",\"message\":\"(.*?)\"(.*)");

        Map<String, List<String>> collect = Files.lines(Paths.get(FILE_PATH))
                .map(p::matcher)
                .filter(Matcher::matches)
                .map(matcher -> new Pair(matcher.group(2), matcher.group(3)))
                .collect(Collectors.groupingBy(Pair::getId, HashMap::new,
                        Collectors.mapping(Pair::getMessage, Collectors.toList())));

        collect.forEach((k, v) -> System.out.println(k + ":\n\t\t" + v.stream().collect(Collectors.joining("\n\t\t"))));

    }

    private static final class Pair {
        private final String id;
        private final String message;

        public Pair(String id, String message) {
            this.id = id;
            this.message = message;
        }

        public String getId() {
            return id;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return message;
        }
    }
}
